part of Game;

class Sprite {
    bool load = false;
    ImageElement image;
    Map imagesMap = new Map();
    Map imagesLoad = new Map();
    
    int counter = 0;
    
    Sprite(images) {
        imagesMap = images;
        imagesMap.forEach((k,v) => loadItem(v, k));
    }
    
    loadItem(String path, String key) {
        image = new ImageElement(src: path);
        image.onLoad.listen((Event e) => onData(e, key, image), onError: onError);
    }
    
    onData(Event e, String key, ImageElement image) {
        counter++; 
        //imagesLoad.putIfAbsent(key, () => image);
        imagesLoad[key] = image;
        print('Loaded ${counter} of ${imagesLoad.length} sprite.');
        if(counter == imagesLoad.length)
            setLoad(true);
    }
    
    onError(Event e) {
        print('Error with loding image: ${e}');
    }
    
    setLoad(bool value) {
        load = value;
    }
    
    get(key) {
        return imagesLoad[key];
    }
    
    getAll() {
        return imagesLoad;
    }
}