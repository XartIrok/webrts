library Game;

import 'dart:html';
import 'dart:math';

part 'sprite.dart';
part 'game.dart';

CanvasElement canvas = querySelector("#game");
CanvasRenderingContext2D context;

Map images = new Map();
Sprite spr;

// init game
void main() {
    context = canvas.getContext('2d');
    
    startUp();
    
    images = {
        'first'     : 'img/first.png',
        'second'    : 'img/second.png'
    };
    
    //test sprite
    spr = new Sprite(images);
    
    window.onResize.listen((event) => resize());
    resize();
}

void startUp() {
    print('------------------------------------');
    print('${Game.nameGame} ${Game.version}');
    print('------------------------------------');
}

void resize() {
    int w = window.innerWidth;
    int h = window.innerHeight;
    
    canvas.setAttribute('width', "${w}px");
    canvas.setAttribute('height', "${h}px");
    
    render();
}

void render() {
    var ctx = context;
    if(spr.load)
        print(spr.get('first'));
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width,  canvas.height);

    if(spr.load)
        ctx.drawImage(spr.get('first'), 0, 0);
}